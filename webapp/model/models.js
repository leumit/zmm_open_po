/* global cOwner : true*/
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"sap/ui/model/Filter"
], function(JSONModel, Device,Filter) {
	"use strict";
 
	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
			createJSONModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
				Filters:{}
			});
			oModel.setSizeLimit(1000);
			return oModel;
		},
		loadTableData: function(sType , sMainLgort){
			debugger;
			var aFilters = [];
			if(sType === 'PURCHASER'){
				aFilters = [new Filter("ZmmBsgrp", sap.ui.model.FilterOperator.EQ, 'GR')];
			}
			else if(sType === 'StockInTransit'){
				aFilters = [new Filter("ZmmBsgrp", sap.ui.model.FilterOperator.EQ, 'TR')];
			}
			else if (sType === 'PHARMACISTS'){
				aFilters = [new Filter({ filters: [new Filter("bsart", sap.ui.model.FilterOperator.EQ, 'ZB'),
												   new Filter("bsart", sap.ui.model.FilterOperator.EQ, 'ZD'),
												   new Filter("bsart", sap.ui.model.FilterOperator.EQ, 'ZU')],
										 and: false}),
							new Filter("knttp", sap.ui.model.FilterOperator.EQ, ''),
							new Filter("werks", sap.ui.model.FilterOperator.EQ, '1000')];
							
			}
			else if(sType === 'PHARMACISTS_USER'){
				aFilters = [new Filter({ filters: [new Filter("bsart", sap.ui.model.FilterOperator.EQ, 'ZB'),
												   new Filter("bsart", sap.ui.model.FilterOperator.EQ, 'ZD'),
												   new Filter("bsart", sap.ui.model.FilterOperator.EQ, 'ZU')],
										 and: false}),
							new Filter("knttp", sap.ui.model.FilterOperator.EQ, ''),
							new Filter("werks", sap.ui.model.FilterOperator.EQ, '1000')];
				if(!!sMainLgort){
					aFilters.push(new Filter("lgort", sap.ui.model.FilterOperator.EQ, sMainLgort));
				}			
				
			}
			// const aFilters = [
			// 	new Filter("IvGroupByVendor", "EQ", true),
			// 	new Filter("Vendor", "EQ", sVendor),
			// 	new Filter("IvVaccineType", "EQ", sType)

			// ];
			// var sPath = cOwner.getModel("ODATA").createKey("/zmm_open_po_list");
			return new Promise(function(resolve, reject) {
				cOwner.getModel("ODATA").read("/ZMM_OPEN_PO_LIST_BI", {
					filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
		},
		loadUserLgortData: function(){
			debugger;
			return new Promise(function(resolve, reject) {
				cOwner.getModel("OPEN_PO").read("/zmm_top_lgort_open_po_by_userSet('')", {
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
		}

	};
});