/* global cOwner : true*/
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"ZMM_OPEN_PO/model/models",
	"sap/ui/export/Spreadsheet",
	"sap/ui/model/Filter",
	'sap/ui/table/TablePersoController',
	"sap/m/MessageBox",
	"ZMM_OPEN_PO/model/formatter"
], function(Controller, models, Spreadsheet ,Filter , TablePersoController,MessageBox,formatter ) {
	"use strict";

	return Controller.extend("ZMM_OPEN_PO.controller.Main", {
		formatter:formatter,
		onInit: function() {
			cOwner._MainController = this; 
			var sType = '';
			try{
				sType = cOwner.getComponentData().startupParameters["TileName"][0];
			}catch (e) {}
			cOwner.getModel("JSON").setProperty("/AppType",sType);
			this._oGlobalFilter = null;
			this._oPriceFilter = null;
			if(sType === 'PHARMACISTS_USER'){
				models.loadUserLgortData().then(function(data) {
					cOwner._MainController.GetData(sType , data.EvMainLgort);
				}).catch(function (error) {
					cOwner._MainController.GetData(sType);
				});
			}
			else{
			cOwner._MainController.GetData(sType);
			}
		},
		onAfterRendering: function () {
			debugger;
			var sKey = cOwner.getModel("JSON").getProperty("/AppType");
			this.oPersonalizationService = sap.ushell.Container.getService("Personalization");

			//const sItem = sap.ushell.services.AppConfiguration.getCurrentApplication().applicationDependencies.name;
			var oPersId = {
				container: sKey + "Personalisation", //any
				item: sKey //any- I have used the table name 
			};
			var oScope = {
				keyCategory: this.oPersonalizationService.constants.keyCategory.FIXED_KEY,
				writeFrequency: this.oPersonalizationService.constants.writeFrequency.LOW,
				clientStorageAllowed: true
			};

			// Get a Personalizer
			var oPersonalizer = this.oPersonalizationService.getPersonalizer(oPersId, oScope, cOwner);

			this._oTPC = new TablePersoController({
				table: this.getView().byId("dataTable"),
				componentName: "ZmmOpenPo" + sKey,
				persoService: oPersonalizer
			});
		},
		GetData: function(sType ,sMainLgort ) {
			debugger;
			
			models.loadTableData(sType , sMainLgort).then(function(data) {
				debugger;
				let aKostl = [],
					aErnam = [];
				for (var i = 0; i < data.results.length; i++) {				
					aKostl.push({"Kostl" :data.results[i].kostl});
					aErnam.push({"Ernam" :data.results[i].ernam});
					data.results[i].totalLinesString = data.results[i].total_lines.toString();
					data.results[i].openlinesString = data.results[i].open_lines.toString();
					data.results[i].sbedat = cOwner._MainController.fixDate(data.results[i].bedat);
				}
				cOwner.getModel("JSON").setProperty("/ErnamArray", cOwner._MainController.removeDuplicates(aErnam, 'Ernam'));
				cOwner.getModel("JSON").setProperty("/KostlArray", cOwner._MainController.removeDuplicates(aKostl, 'Kostl'));
				cOwner.getModel("JSON").setProperty("/count", data.results.length);
				cOwner.getModel("JSON").setProperty("/orders", data.results);
			}).catch(function (error) {
				try {
					MessageBox.error(JSON.parse(error.responseText).error.message.value);
				} catch (e) {
					MessageBox.error(JSON.stringify(error));
				}
				console.log(error);
			});
		
		},
		removeDuplicates: function(array , key){
			let lookup = new Set();
			return array.filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]));
		},
		fixDate: function(oDate) {
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.yyyy"
			});
			var sNewDate = dateFormat.format(oDate);
			return sNewDate;
		},
		_filter: function() {
			var oFilter = null;
			if (this._oGlobalFilter && this._oPriceFilter) {
				oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
			} else if (this._oGlobalFilter) {
				oFilter = this._oGlobalFilter;
			} else if (this._oPriceFilter) {
				oFilter = this._oPriceFilter;
			}

			cOwner._MainController.byId("dataTable").getBinding("rows").filter(oFilter, "Application");
			cOwner.getModel("JSON").setProperty("/count", cOwner._MainController.byId("dataTable").getBinding("rows").getLength());
		},

		filterGlobally: function(oEvent) {
			this._oGlobalFilter = null;
			var oFilters = [];
			var model = cOwner.getModel("JSON"),
				fData = model.getProperty("/Filters");
			if (fData.ErnamFilter)
				oFilters.push(new sap.ui.model.Filter("ernam", "Contains", fData.ErnamFilter));
			if (fData.DocDateFrom || fData.DocDateTo) {
				if (!fData.DocDateFrom) { // only to
					oFilters.push(new sap.ui.model.Filter("bedat", "LE", fData.DocDateTo));
				} else if (!fData.DocDateTo) { // only from
					oFilters.push(new sap.ui.model.Filter("bedat", "GE", fData.DocDateFrom));
				} else { // from && to
					oFilters.push(new sap.ui.model.Filter("bedat", "BT", fData.DocDateFrom, fData.DocDateTo));
				}
			}
			if (fData.Matnr && fData.Matnr.length) {
				var aMatnr =[];
					for (const element of fData.Matnr) {
						aMatnr.push(new sap.ui.model.Filter("matnr", "EQ", element.key));
					}
				oFilters.push(new Filter(aMatnr,false));
			}	
			if (fData.Lgort && fData.Lgort.length) {
				var aLgort = [];
				for (const element of fData.Lgort) {
					aLgort.push(new sap.ui.model.Filter("lgort", "EQ", element.key));
				}
				oFilters.push(new Filter(aLgort, false));
			}	
			if (fData.Bsart && fData.Bsart.length) {
				var aBsart = [];
				for (const element of fData.Bsart) {
					aBsart.push(new sap.ui.model.Filter("bsart", "EQ", element.key));
				}
				oFilters.push(new Filter(aBsart, false));
			}	
			if (oFilters.length !== 0) {
				this._oGlobalFilter = new Filter(oFilters, true);
			}

			this._filter();
		},

		clearAllFilters: function(oEvent) {
			var oTable = cOwner._MainController.byId("dataTable");
			this._oGlobalFilter = null;
			this._oPriceFilter = null;
			this._filter();
			var aColumns = oTable.getColumns();
			for (var i = 0; i < aColumns.length; i++) {
				oTable.filter(aColumns[i], null);
				oTable.sort(aColumns[i], null);
			}
			cOwner.getModel("JSON").setProperty("/Filters", []);
		},
		
		getTableFilters: function () {
			var oTable = cOwner._MainController.byId("dataTable");
			var oColumns = oTable.getColumns();
			var aFilters = [];
			for (var i = 0; i < oColumns.length; i++) {
				var sValue = oColumns[i].getFilterValue();
				if (sValue) {
					aFilters.push({
						value: sValue,
						property: oColumns[i].getFilterProperty()
					});
				}
			}
			return aFilters;
		},
		setTableFilters: function (tableFilters) {
			var oTable = cOwner._MainController.byId("dataTable");
			var oColumns = oTable.getColumns();
			for (var i = 0; i < oColumns.length; i++) {
				for (var j = 0; j < tableFilters.length; j++) {
					if (tableFilters[j].property === oColumns[i].getFilterProperty()) {
						oColumns[i].filter(tableFilters[j].value);
						continue;
					}
				}
			}
		},
		createColumnConfig: function() {
			var aCols = cOwner._MainController.byId("dataTable").getColumns();
			var aColumns = [];
			for (var i = 0; i < aCols.length; i++) {
				if (cOwner._MainController.byId("dataTable").getColumns()[i].getVisible()) {
					aColumns.push({
						label: aCols[i].getLabel().getProperty("text") ||" ",
						property: aCols[i].getFilterProperty()|| '',
						width: '15rem'
					});
				}
			}
			return aColumns;

		},
		onExport: function() {
			var aCols, aItems, oSettings, oSheet;
			aCols = this.createColumnConfig();
			aItems = cOwner.getModel("JSON");
			var aIndices = cOwner._MainController.byId("dataTable").getBinding("rows").aIndices;
			var aSelectedModel = [];
			for (var i = 0; i<aIndices.length; i++){
				aSelectedModel.push(aItems.getProperty("/orders/" + aIndices[i] ));
			}
			// var aSelectedModel = cOwner._MainController.byId("dataTable").getBinding("rows").oList;
			oSettings = {
				workbook: {
					columns: aCols
				},
				dataSource: aSelectedModel,
				fileName: cOwner.getModel("JSON").getProperty("/ExportName")
			};

			oSheet = new Spreadsheet(oSettings);
			oSheet.build()
				.then(function() {});

		},
		showDialog: function (sDialogName) {
			if (!this[sDialogName]) {
				this[sDialogName] = sap.ui.xmlfragment("ZMM_OPEN_PO.view.fragments." + sDialogName +'Dialog', this);
				this.getView().addDependent(this[sDialogName]);
			}
			this[sDialogName].open();
		},
		closeDialog: function (sDialogName) {
			this[sDialogName]._dialog.close();
			// this[sDialogName].destroy();
			// delete(this[sDialogName]);
		},
		onConfirm: function (oEvent , sType) {
			const aSelected = oEvent.getParameter("selectedItems");
			const model = cOwner.getModel("JSON");
			var aTokens = this.CreateToken(aSelected, sType, "SH");
			// for (var i = 0; i < aTokens.length; i++) {
			// 	var object = aSelected[i].getBindingContext("SH").getObject();
			// 	aTokens[i].StorageLocation = object.Lgort;
			// }
			model.setProperty("/Filters/" + sType, aTokens);
			this.closeDialog(oEvent);
		},
		search: function (oEvent , sType) {
			const value = oEvent.getParameter("value");
			var oFilter;
			if(sType === 'Lgort'){
				oFilter = new Filter({
				filters: [new Filter("Lgort", "Contains", value.toUpperCase()),
					new Filter("Lgobe", "Contains", value)
				],
				and: false
			});
		}
		else if(sType === 'Matnr'){
			oFilter = new Filter({
				filters: [new Filter("Matnr", "Contains", value.toUpperCase()),
					new Filter("Maktx", "Contains", value)
				],
				and: false
			});
		}
		else if(sType === 'Bsart'){
			oFilter = new Filter({
				filters: [new Filter("Bsart", "Contains", value.toUpperCase()),
					new Filter("Batxt", "Contains", value.toUpperCase())
				],
				and: false
			});
		}
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},
		CreateToken: function (aSelected, sKey, sModel) {
			var aTokens = [];
			for (var i = 0; i < aSelected.length; i++) {
				var object = aSelected[i].getBindingContext(sModel).getObject();
			if(sKey === 'Lgort'){
				aTokens.push({
					text: object['Lgobe'],
					key: object['Lgort']});
			}
			else if(sKey === 'Matnr'){
				aTokens.push({
					text: object['Maktx'],
					key: object['Matnr']
				});
			}
			else if(sKey === 'Bsart'){
				aTokens.push({
					text: object['Bstyp'],
					key: object['Bsart']
				});
			}
		}
			return aTokens;
			
		},
		onDeleteToken: function (oEvent, sPath) {
			debugger;
			const sKey = oEvent.getParameter("token").getBindingContext("JSON").getObject().key;
			const model = cOwner.getModel("JSON");
			var aTokens = model.getProperty("/Filters/" + sPath);
			const index = this.findIndexToDelete(aTokens, sKey);
			aTokens.splice(index, 1);
			model.setProperty("/Filters/" + sPath, aTokens);
			// this._filter();
		},
		findIndexToDelete: function (aTokens, sKey) {
			for (var i = 0; i < aTokens.length; i++) {
				if (sKey === aTokens[i].key) {
					return i;
				}
			}
		},

		onPersoButtonPressed: function (oEvent) {
			cOwner._MainController._oTPC.openDialog();
		}
	});
});